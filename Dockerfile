FROM alpine:3.9

# version tags
ARG GRPC_RELEASE_TAG=v1.19.1
ARG GRPC_WEB_RELEASE=https://github.com/grpc/grpc-web/releases/download/1.0.4/protoc-gen-grpc-web-1.0.4-linux-x86_64
ARG GOOGLE_TEST_RELEASE_TAG=release-1.8.1

RUN apk add --no-cache \
  autoconf pkgconfig libtool automake g++ make clang git zlib-dev curl-dev util-linux-dev cmake gdb 

# Install googletest
RUN    echo "--- installing Google test ---" && \
    git clone -b ${GOOGLE_TEST_RELEASE_TAG} --single-branch  https://github.com/google/googletest.git /var/local/src/gtest && \
    cd /var/local/src/gtest && \
    git submodule update --init --recursive && \
    mkdir _bld && cd _bld && \
    cmake .. && make -j$(nproc) && make install


